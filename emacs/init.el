(use-package avy
  :ensure t
  :custom-face
  (avy-lead-face ((t (:foreground "black"))))
  (avy-lead-face-0 ((t (:foreground "black")))))

(use-package consult
  :ensure t)

(use-package expand-region
  :ensure t)

(use-package multiple-cursors
  :ensure t)

(use-package vertico
  :ensure t
  :init
  (vertico-mode))

(use-package generic-x)

(cond ((eq system-type 'darwin)
       (add-to-list 'major-mode-remap-alist '(perl-mode . cperl-mode))

       (use-package company
         :ensure t
         :hook (after-init . global-company-mode)))
      ((eq system-type 'gnu/linux)
       (use-package dockerfile-mode
         :ensure t)))

(column-number-mode t)
(global-auto-revert-mode t)
(global-display-line-numbers-mode t)
(global-hl-line-mode t)
(save-place-mode t)
(savehist-mode t)
(show-paren-mode t)

(bind-keys ("C-h"   . avy-goto-line)
           ("C-<"   . mc/mark-previous-like-this)
           ("C->"   . mc/mark-next-like-this)
           ("C-="   . er/expand-region)
           ("C-x b" . consult-buffer)
           ("C-c a" . align)
           ("C-c d" . delete-trailing-whitespace)
           ("C-c i" . indent-region)
           ("C-c s" . sort-lines)
           ("C-c t" . tabify)
           ("C-c u" . untabify))
