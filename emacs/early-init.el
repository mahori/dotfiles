(when load-file-name
  (setq user-emacs-directory (file-name-directory load-file-name)))

(setq custom-file (locate-user-emacs-file "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

(require 'package)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(cond ((eq system-type 'darwin)
       (setq default-frame-alist '((width  . 160)
                                   (height . 80)))
       (set-face-attribute 'default nil :family "PlemolJP" :height 140)
       (scroll-bar-mode 0)
       (tool-bar-mode 0)

       (use-package ef-themes
         :ensure t
         :init
         (load-theme 'ef-dream t)
         ))
      (t
       (menu-bar-mode 0)))

(setq-default indent-tabs-mode nil)

(setq history-delete-duplicates t)
(setq history-length (* history-length 10))
(setq inhibit-startup-screen t)
