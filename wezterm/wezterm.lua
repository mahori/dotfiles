local wezterm = require 'wezterm'

local config = wezterm.config_builder()

config.bold_brightens_ansi_colors = false
config.check_for_updates = false
config.color_scheme = 'Dark+'
config.default_cwd = wezterm.home_dir
config.default_prog = { '/usr/local/bin/fish', '-l' }
config.font = wezterm.font( 'PlemolJP Console NF' )
config.font_size = 14
config.initial_cols = 135
config.initial_rows = 75
config.selection_word_boundary = ' \t\n{}[]()"\'`*'
config.underline_position = -4

config.keys = {
   { key = 'Enter', mods = 'ALT',        action = wezterm.action.SendKey( { key = 'Enter', mods = 'ALT' } ) },
   { key = '=',     mods = 'CTRL',       action = wezterm.action.SendString( '\x18@c=' )                    },
   { key = '<',     mods = 'SHIFT|CTRL', action = wezterm.action.SendString( '\x18@c<' )                    },
   { key = '>',     mods = 'SHIFT|CTRL', action = wezterm.action.SendString( '\x18@c>' )                    }
}

config.mouse_bindings = {
   {
      event = { Up = { streak = 1, button = 'Left' } },
      mods = 'NONE',
      action = wezterm.action.CompleteSelection( 'PrimarySelection' )
   },
   {
      event = { Up = { streak = 1, button = 'Left' } },
      mods = 'CTRL',
      action = wezterm.action.OpenLinkAtMouseCursor
   }
}

return config
