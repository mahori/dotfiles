alias cp "cp -i"
alias less "less -R"
alias mv "mv -i"
alias rm "rm -i"
alias su "su -l"
alias tmux "tmux -2"
alias where "command -v"

set -gx BLOCKSIZE "K"
set -gx CPAN_HOME "$HOME/.local/share/perl"
set -gpx PERL5LIB "$CPAN_HOME/lib/perl5"
set -gpx PERL_LOCAL_LIB_ROOT "$CPAN_HOME"
set -gx PERL_MB_OPT "--install_base $CPAN_HOME"
set -gx PERL_MM_OPT "INSTALL_BASE=$CPAN_HOME"
set -gx PYTHONUSERBASE "$HOME/.local/share/python"
set -gx _Z_DATA "$__fish_user_data_dir/z"

fish_add_path $PYTHONUSERBASE/bin
fish_add_path $CPAN_HOME/bin
fish_add_path $HOME/.cargo/bin
fish_add_path $HOME/bin

# fzf
function fzf_ghq
    ghq list -p | fzf | read line
    if test $line
        cd $line
        commandline -f repaint
    else
        commandline ''
    end
end
function fzf_history
    history | fzf | read line
    if test $line
        commandline $line
    else
        commandline ''
    end
end
function fzf_z
    z -l | fgrep -v 'common:' | sed -E -e 's/^[.0-9]+ +//' | fzf | read line
    if test $line
        cd $line
        commandline -f repaint
    else
        commandline ''
    end
end

function fish_user_key_bindings
    bind \cd delete-char
    bind \cr fzf_history
    bind \c] fzf_ghq
    bind \x1c fzf_z
end

if ! fish_is_root_user
    set -gx LANG "ja_JP.UTF-8"
end

switch (uname)
    case Darwin
        source $__fish_config_dir/darwin.fish
    case FreeBSD
        source $__fish_config_dir/freebsd.fish
    case Linux
        source $__fish_config_dir/linux.fish
end

if test -f $__fish_config_dir/private.fish
    source $__fish_config_dir/private.fish
end
