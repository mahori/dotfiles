set -gx GIT_EDITOR "codium --wait"
set -gx GPG_TTY (tty)

# Alacritty
function an
    switch (count $argv)
        case 0
            open --new -a Alacritty.app \
                 --args \
                 $argv
        case '*'
            open --new -a Alacritty.app \
                 --args \
                 --title $argv[-1] \
                 --command slogin $argv
    end
end
function ao
    switch (count $argv)
        case 0
            open --new -a Alacritty.app \
                 --args \
                 --option window.opacity=0.9 \
                 $argv
        case '*'
            open --new -a Alacritty.app \
                 --args \
                 --option window.opacity=0.9 \
                 --title $argv[-1] \
                 --command slogin $argv
    end
end

# Emacs
alias sn "$HOME/Applications/Emacs.app/Contents/MacOS/Emacs"
alias sw "$HOME/Applications/Emacs.app/Contents/MacOS/Emacs -g 240x80"
alias sm "$HOME/Applications/Emacs.app/Contents/MacOS/Emacs -e toggle-frame-maximized"

# Visual Studio Code
alias en "code --new-window"
alias er "code --reuse-window"

# VSCodium
alias mn "codium --new-window"
alias mr "codium --reuse-window"
