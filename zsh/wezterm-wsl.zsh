function wn() {
    wezterm-gui.exe \
        --config initial_cols=200 \
        --config initial_rows=90 \
        ${@:+ssh} ${@:+"$@"}
}

function wo() {
    wezterm-gui.exe \
        --config initial_cols=200 \
        --config initial_rows=90 \
        --config window_background_opacity=0.80 \
        ${@:+ssh} ${@:+"$@"}
}
