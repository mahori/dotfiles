export EDITOR='emacs'
export GPG_TTY="$(tty)"
export PATH="${HOME}/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin"

case ${UID} in
    0)
        export LANG='C.UTF-8'
        ;;
    *)
        export LANG='ja_JP.UTF-8'
        ;;
esac

alias ls='ls -CFG'
alias ll='ls -Alo'
