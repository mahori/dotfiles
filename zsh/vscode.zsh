function en() {
    code --new-window "$@"
}

function er() {
    code --reuse-window "$@"
}

function esn() {
    local host=$1
    shift
    code --new-window --remote ssh-remote+"${host}" "$@"
}

function esr() {
    local host=$1
    shift
    code --reuse-window --remote ssh-remote+"${host}" "$@"
}

function mn() {
    codium --new-window "$@"
}

function mr() {
    codium --reuse-window "$@"
}
