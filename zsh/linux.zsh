export EDITOR='emacs'
export GPG_TTY="$(tty)"
export CPAN_HOME="${HOME}/.local/share/perl"
export PERL5LIB="${CPAN_HOME}/lib/perl5${PERL5LIB:+:}${PERL5LIB}"
export PERL_LOCAL_LIB_ROOT="${CPAN_HOME}${PERL_LOCAL_LIB_ROOT:+:}${PERL_LOCAL_LIB_ROOT}"
export PERL_MB_OPT="--install_base ${CPAN_HOME}"
export PERL_MM_OPT="INSTALL_BASE=${CPAN_HOME}"
export PYTHONUSERBASE="${HOME}/.local/share/python"
export GEM_HOME="${HOME}/.local/share/ruby"
export LD_LIBRARY_PATH="/usr/local/lib${LD_LIBRARY_PATH:+:}${LD_LIBRARY_PATH}"
export PATH="${HOME}/bin:${CPAN_HOME}/bin:${PYTHONUSERBASE}/bin:${GEM_HOME}/bin:${PATH}"

if [ ${UID} -ge 1000 ] ; then
    export LANG='ja_JP.UTF-8'
    if [ -n "${WSLENV}" ] ; then
        export GIT_EDITOR='codium --wait'
        source ${HOME}/.zsh/wezterm-wsl.zsh

        ${HOME}/bin/start-ssh-agent
        source ${HOME}/.zsh/ssh-agent.zsh > /dev/null 2>&1
    else
        export GIT_EDITOR='emacs'
    fi
else
    export LANG='C.UTF-8'
fi

alias ls='ls -CF --color'
alias ll='ls -Al'
