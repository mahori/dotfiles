function sn() {
    "${HOME}/Applications/Emacs.app/Contents/MacOS/Emacs" "$@"
}

function sw() {
    "${HOME}/Applications/Emacs.app/Contents/MacOS/Emacs" -g 240x80 "$@"
}

function sm() {
    "${HOME}/Applications/Emacs.app/Contents/MacOS/Emacs" -e 'toggle-frame-maximized' "$@"
}
