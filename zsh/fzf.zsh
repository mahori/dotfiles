function fzf_cdr() {
    local selected_dir="$(cdr -l | sed -E -e 's/^[0-9]+ +//' | fzf --prompt='cdr > ' --query "$LBUFFER")"

    if [ -n "$selected_dir" ] ; then
        BUFFER="cd ${selected_dir}"

        zle accept-line
    fi
}

function fzf_ghq() {
    local selected_dir="$(ghq list -p | sed -E -e "s#^${HOME}#~#" | fzf --prompt='repositories > ' --query "$LBUFFER")"

    if [ -n "$selected_dir" ] ; then
        BUFFER="cd ${selected_dir}"

        zle accept-line
    fi
}

function fzf_history() {
    BUFFER="$(history -n -r 1 | fzf --prompt='history > ' --query "$LBUFFER")"
    CURSOR=$#BUFFER
}

zle -N fzf_cdr
zle -N fzf_ghq
zle -N fzf_history

bindkey '^|' fzf_cdr
bindkey '^]' fzf_ghq
bindkey '^R' fzf_history
