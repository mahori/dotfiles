function wn() {
    open --new -a WezTerm.app \
         --args \
         --config initial_cols=200 \
         --config initial_rows=80 \
         ${@:+ssh} ${@:+"$@"}
}

function wo() {
    open --new -a WezTerm.app \
         --args \
         --config initial_cols=200 \
         --config initial_rows=80 \
         --config window_background_opacity=0.80 \
         ${@:+ssh} ${@:+"$@"}
}
