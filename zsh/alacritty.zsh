function an() {
    open --new -a Alacritty.app \
         --args \
         ${@:+--title} ${@:+${@:$#}} \
         ${@:+--command} ${@:+slogin} ${@:+"$@"}
}

function ao() {
    open --new -a Alacritty.app \
         --args \
         --option window.opacity=0.9 \
         ${@:+--title} ${@:+${@:$#}} \
         ${@:+--command} ${@:+slogin} ${@:+"$@"}
}
