export BLOCKSIZE='K'
export HISTFILE="${HOME}/.config/zsh/history"
export HISTSIZE=5000000
export PAGER='less'
export SAVEHIST=1000000

setopt hist_expire_dups_first
setopt hist_find_no_dups
setopt hist_ignore_all_dups
setopt hist_ignore_dups
setopt hist_save_no_dups
setopt ignore_eof
setopt share_history

stty start undef
stty stop undef

alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'
alias su='su -l'

# cdr
autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
add-zsh-hook chpwd chpwd_recent_dirs
zstyle ':chpwd:*' recent-dirs-file ${HOME}/.config/zsh/chpwd-recent-dirs +
zstyle ':chpwd:*' recent-dirs-max 2000

# smart-insert-last-word
autoload -Uz smart-insert-last-word
zle -N insert-last-word smart-insert-last-word

source ${HOME}/.config/zsh/ohmyzsh.zsh
