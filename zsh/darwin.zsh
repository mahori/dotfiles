export EDITOR='codium'
export GIT_EDITOR='codium --wait'
export GPG_TTY="$(tty)"
export CPAN_HOME="${HOME}/.local/share/perl"
export PERL5LIB="${CPAN_HOME}/lib/perl5${PERL5LIB:+:}${PERL5LIB}"
export PERL_LOCAL_LIB_ROOT="${CPAN_HOME}${PERL_LOCAL_LIB_ROOT:+:}${PERL_LOCAL_LIB_ROOT}"
export PERL_MB_OPT="--install_base ${CPAN_HOME}"
export PERL_MM_OPT="INSTALL_BASE=${CPAN_HOME}"
export PYTHONUSERBASE="${HOME}/.local/share/python"
export POETRY_HOME="${HOME}/.local/share/poetry"
export GEM_HOME="${HOME}/.local/share/ruby"

paths=(
    ${HOME}/bin(N-/)
    ${HOME}/.cargo/bin(N-/)
    ${CPAN_HOME}/bin(N-/)
    ${PYTHONUSERBASE}/bin(N-/)
    ${POETRY_HOME}/bin(N-/)
    ${GEM_HOME}/bin(N-/)
    /usr/local/opt/python/bin(N-/)
    /usr/local/opt/ruby/bin(N-/)
    /usr/local/opt/mysql-client/bin(N-/)
    /usr/local/bin(N-/)
    /usr/bin(N-/)
    /bin(N-/)
    /usr/local/sbin(N-/)
    /usr/sbin(N-/)
    /sbin(N-/)
)
export PATH="${(j/:/)paths}"
unset paths

if [ ${UID} -ge 500 ] ; then
    export LANG='ja_JP.UTF-8'
    source ${HOME}/.config/zsh/alacritty.zsh
    source ${HOME}/.config/zsh/emacs.zsh
    source ${HOME}/.config/zsh/fzf.zsh
    source ${HOME}/.config/zsh/man2pdf.zsh
    source ${HOME}/.config/zsh/vscode.zsh
    source ${HOME}/.config/zsh/wezterm-darwin.zsh
else
    export LANG='C'
    export LC_CTYPE='UTF-8'
fi

alias ls='ls -CF'
alias ll='ls -AOl'
