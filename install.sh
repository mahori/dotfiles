#!/bin/sh

# emacs
echo '===> emacs'
if [ ! -e "${HOME}/.emacs.d" ] ; then
    ln -s ${PWD}/emacs ${HOME}/.emacs.d
fi

# tmux
echo '===> tmux'
if [ ! -e "${HOME}/.tmux.conf" ] ; then
    ln -s ${PWD}/tmux/tmux.conf ${HOME}/.tmux.conf
fi
if [ ! -e "${HOME}/.tmux" ] ; then
    ln -s ${PWD}/tmux ${HOME}/.tmux
fi
if [ ! -e "${HOME}/.tmux/plugins/tpm" ] ; then
    git clone https://github.com/tmux-plugins/tpm.git ${HOME}/.tmux/plugins/tpm
fi

# xdg
echo '===> xdg'
if [ ! -e "${HOME}/.config" ] ; then
    mkdir ${HOME}/.config
fi

# git
echo '===> git'
if [ ! -e "${HOME}/.config/git" ] ; then
    ln -s ${PWD}/git ${HOME}/.config/git
fi

# fish
if [ -e /usr/bin/fish -o -e /usr/local/bin/fish ] ; then
    echo '===> fish'
    if [ ! -e "${HOME}/.config/fish" ] ; then
        ln -s ${PWD}/fish ${HOME}/.config/fish
    fi
fi

# zsh
if [ -e /bin/zsh -o -e /usr/local/bin/zsh ] ; then
    echo '===> zsh'
    if [ ! -e "${HOME}/.zshrc" ] ; then
        ln -s ${PWD}/zsh ${HOME}/.config/zsh
        ln -s ${PWD}/zsh/zshrc ${HOME}/.zshrc
    fi
    if [ ! -e "${HOME}/.config/zsh/ohmyzsh" ] ; then
        export ZSH="${HOME}/.config/zsh/ohmyzsh"
        export CHSH='no'
        export RUNZSH='no'
        export KEEP_ZSHRC='yes'
        if command -v curl > /dev/null 2>&1 ; then
            sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
        elif command -v wget > /dev/null 2>&1 ; then
            sh -c "$(wget -q -O - https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
        fi
        git clone https://github.com/zsh-users/zsh-autosuggestions.git ${ZSH}/custom/plugins/zsh-autosuggestions
        git clone https://github.com/zsh-users/zsh-completions.git ${ZSH}/custom/plugins/zsh-completions
        git clone https://github.com/zsh-users/zsh-history-substring-search.git ${ZSH}/custom/plugins/zsh-history-substring-search
        git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH}/custom/plugins/zsh-syntax-highlighting
    fi
fi

if [ "$(uname -s)" = 'Darwin' ] ; then
    # fish
    echo '===> fish #2'
    if [ ! -e "${HOME}/.config/fish/functions/fisher.fish" ] ; then
        fish -c 'curl -fsSL https://git.io/fisher | source && fisher install jorgebucaran/fisher'
    fi

    # alacritty
    echo '===> alacritty'
    if [ ! -e "${HOME}/.config/alacritty" ] ; then
        ln -s ${PWD}/alacritty ${HOME}/.config/alacritty
    fi
    if [ ! -e "${HOME}/.config/alacritty/themes" ] ; then
        git clone https://github.com/alacritty/alacritty-theme.git ${HOME}/.config/alacritty/themes
    fi

    # ghostty
    echo '===> ghostty'
    if [ ! -e "${HOME}/.config/ghostty" ] ; then
        ln -s ${PWD}/ghostty ${HOME}/.config/ghostty
    fi

    # wezterm
    echo '===> wezterm'
    if [ ! -e "${HOME}/.config/wezterm" ] ; then
        ln -s ${PWD}/wezterm ${HOME}/.config/wezterm
    fi
fi
